/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;

/**
 *
 * @author ASUS
 */
public class Triangle extends Shape {

    private double width, height;

    public Triangle(double width, double height) {
        super();
        System.out.println("Triangle create");
        this.width = width;
        this.height = height;
    }

    @Override
    public double calArea() {
        return 0.5 * width * height;
    }

    public void print() {
        System.out.println("Circle : width : " + width + " height : " + height + ", arear = " + this.calArea());
    }

}
