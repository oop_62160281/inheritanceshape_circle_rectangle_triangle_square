/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;

/**
 *
 * @author ASUS
 */
public class Rectangle extends Shape {

    double width;
    private double height;

    public Rectangle(double width, double height) {
        super();
        System.out.println("Rectangle create");
        this.width = width;
        this.height = height;
    }

    @Override
    public double calArea() {
        return width * height;
    }

    public void print() {
        System.out.println("Rectangle : width : " + width + " height : "
                + height + ", arear = " + this.calArea());
    }

}
