/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;

/**
 *
 * @author ASUS
 */
public class TestShape {

    public static void main(String[] args) {
        Circle circle1 = new Circle(3);
        circle1.print();

        Rectangle rectangle = new Rectangle(4, 6);
        rectangle.print();

        Square square = new Square(4);
        square.print();
        
        Triangle triangle =  new Triangle(2,4);
        triangle.print();


    }

}
